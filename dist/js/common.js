
function changeCalendar () {
	var calendar = $("#contactoReserva #fechaDatepicker");
	if( calendar.val=="" ) {
		calendar.removeClass("focus");
		calendar.siblings().removeClass("selected");
	} else {
		calendar.addClass("focus");
		calendar.siblings().addClass("selected");
	}
}

$( window ).on( "load", function() {

	//Header scroll Effect
	window.addEventListener('scroll', function(e){
		var distanceY = window.pageYOffset || document.documentElement.scrollTop,
			shrinkOn = 8,
			header = $("header.header .wrapper");
		if (distanceY > shrinkOn) {
			header.addClass("scrolled");
		} else {
			if(header.hasClass("scrolled")){
				header.removeClass("scrolled");
			}
		}
	});

	//Responsive menu
	$("span.hamburguesa").on("click", function () {
		var menu = $("ul.menu");

		if( menu.hasClass("MenuOpen") ) {
			menu.removeClass("MenuOpen").addClass("MenuClose");
		} else {
			menu.removeClass("MenuClose").addClass("MenuOpen");
		}
	});

	//Form select effect
	$(".selectEffect").each(function(){
		if( $(this).val()!="" ){
			$(this).siblings().addClass("selected");
		}
	});

	$(".selectEffect").on("focus", function () {
		selectEffect($(this));
	});
	$(".selectEffect").on("focusout", function () {
		selectEffect();
	});
	function selectEffect (x) {
		$(".selectEffect").each(function(){
			$(this).removeClass("focus");
			if( $(this).val()=="" ){
				$(this).siblings().removeClass("selected");
			}
		});
		if (x) {
			x.addClass("focus");
			x.siblings().addClass("selected");
		}
	}

	//Enable Calendar in form
	$( "#fechaDatepicker" ).datepicker();


//////Testing only, not for production ///////////////////////////////////
// 	behaviour for home form
	imgFolder="img/";

	//Set behaviour to selectpicker
	if($(".selectpicker").length>=1){
		setCurrencyFlagUrl();
		$(".selectpicker").on('change', function() {
			setCurrencyFlagUrl();
		});
	}

	if( $(".reservaDivisa").length>=1 ) {
		$(".reservaDivisa .promoOptions").hide();
		$(".reservaDivisa .error").hide();
		$(".reservaDivisa .correct").hide();
		$(".reservaDivisa  .promoLink").on("click", function () {
			$(".reservaDivisa  .promoOptions").toggle()
		});
		$(".reservaDivisa  .sendPromo").on("click", function () {
			if( $(".reservaDivisa .promoCode").val()=="error" ){
				$(".reservaDivisa  .error").show();
				$(".reservaDivisa  .correct").hide();
				$(".reservaDivisa  .correctCode").removeClass("green");
			} else {
				$(".reservaDivisa  .correct").show();
				$(".reservaDivisa  .error").hide();
				$(".reservaDivisa  .correctCode").addClass("green");
			}
		});

	}

	//OFFICE SEARCHER
	if( $(".oficinas").length>=1 ){
		$(".mapZone .cardList .noresults").hide();

		$(".oficinas .adressSearcher a.find").on("click", function(){
			if( $(".mapZone .cardList .noresults").is(':visible') ){
				$(".mapZone .cardList .noresults").hide();
				$(".mapZone .cardList .vcardLocation").show();
			} else {
				$(".mapZone .cardList .noresults").show();
				$(".mapZone .cardList .vcardLocation").hide();
			}
		})
	}

	//FUNCTIONS
	function setCurrencyFlagUrl() {
		$(".selectpicker option").each(function(){
			var currencyClass = $(this).attr("data-icon");
			$(".bootstrap-select").find("."+currencyClass).css("background-image", "url("+imgFolder+currencyClass+".jpg)");
		});
	}
////////////////////////////////////////////////////////////////////////

});